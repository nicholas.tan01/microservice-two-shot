function ShoesList(props){
    return (
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>Name</th>
                </tr>
            </thead>
            <tbody>
                {props.shoes.map(shoe => {
                    return(
                        <tr key={shoe.href}>
                            <td>{ shoe.name }</td>
                        </tr>
                    );
                })}
            </tbody>
        </table>
    );
}

export default ShoesList;