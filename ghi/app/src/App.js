import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import ShoesList from './ShoesList';
import Nav from './Nav';

function App(props) {
//   if (props.shoes === undefined){
//     return null;
//   }
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="shoes">
            <Route path="" element={<ShoesList shoes={props.shoes} />} />
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
