from django.contrib.auth.models import AbstractUser
from django.db import models

# Create your models here.


class Shoe(models.Model):
    name = models.CharField(max_length=40)

    def __str__(self):
        return self.name

    class Meta:
        ordering = ("name",)