from django.http import JsonResponse
from django.shortcuts import render
from django.views.decorators.http import require_http_methods
from .models import Shoe
# Create your views here.
@require_http_methods(["GET"])
def api_list_shoes(request):
    shoes = Shoe.objects.all().order_by('name')
    shoe_list = []
    for shoe in shoes:
        d = {
            "name": shoe.name
        }
        shoe_list.append(d)
    return JsonResponse({"shoes": shoe_list})